import math
def factors(n):
    s=math.floor(math.sqrt(n))
    factors=[]
    for i in range(1,s+1):
        if n%i==0:
            factors.append(i)
            factors.append(int(n/i))
    factors.sort()
    return factors
t=0
i=0
while len(factors(t))<500:
    i+=1
    t+=i
print(t)