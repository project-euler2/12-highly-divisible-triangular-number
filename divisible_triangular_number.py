import math
def triangular_number_divisor(n_wanted_divisors):
    number_found = False
    i = 1
    triangular_number = 0
    while number_found == False:
        n_divisors = 0
        triangular_number += i
        last_divisor = 0
        for j in range(math.ceil(triangular_number/2), 1, -1):
            if (j ** 2) == triangular_number:
                n_divisors = (n_divisors * 2)
                break
            if triangular_number % j == 0:
                #print(str(triangular_number) + '  ' + str(j))
                if (last_divisor * j) == triangular_number:
                    n_divisors = (n_divisors * 2)
                    break
                else:
                    n_divisors +=1
                    last_divisor = j
        i += 1
        n_divisors += 1
        if n_divisors >= n_wanted_divisors:
            print(i)
            number_found = True
    return triangular_number

n = 100
print('n of divisors : '+ str(n) + ' number: ' + str(triangular_number_divisor(n)))